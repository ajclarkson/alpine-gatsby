FROM mhart/alpine-node:8.0.0

RUN apk add --no-cache make gcc g++ python

# If you need npm, don't use a base tag
RUN npm install -g gatsby@0.x.x
